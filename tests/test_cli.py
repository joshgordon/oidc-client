import json
import os
import signal
from subprocess import PIPE, Popen


def test__get_provider_meta():
    with Popen(
        ["python", "-m" "oidc_client", "get-provider-metadata", "https://gitlab.com"],
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        assert proc.stderr.read() == b""

        data = json.load(proc.stdout)
        assert data["issuer"] == "https://gitlab.com"
        assert "authorization_endpoint" in data
        assert "token_endpoint" in data


def test__get_provider_meta_error():
    with Popen(
        ["python", "-m" "oidc_client", "get-provider-metadata", "https://unknown"],
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        assert proc.stdout.read() == b""
        assert b"Failed to query provider configuration" in proc.stderr.read()


def test__login():
    with Popen(
        [
            "python",
            "-m" "oidc_client",
            "--config-file",
            "./tests/profiles.toml",
            "--profile",
            "test_gitlab",
            "--debug",
            "login",
            "--client-secret-stdin",
        ],
        stdin=PIPE,
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        stdout, stderr = proc.communicate(os.getenv("TEST_OIDC_CLIENT_SECRET").encode())
        assert stderr == b""

        data = json.loads(stdout)
        assert isinstance(data.get("access_token"), str)


def test__login_missing_client_secret():
    with Popen(
        [
            "python",
            "-m" "oidc_client",
            "--config-file",
            "./tests/profiles.toml",
            "--profile",
            "test_gitlab",
            "--debug",
            "login",
        ],
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        assert proc.stdout.read() == b""
        assert b"Login configuration error:" in proc.stderr.read()


def test__login_bad_client_id():
    with Popen(
        [
            "python",
            "-m" "oidc_client",
            "--config-file",
            "./tests/profiles.toml",
            "--profile",
            "test_gitlab",
            "--debug",
            "login",
            "--client-secret-stdin",
            "--client-id",
            "BAD_ID",
        ],
        stdin=PIPE,
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        stdout, stderr = proc.communicate(os.getenv("TEST_OIDC_CLIENT_SECRET").encode())
        assert b"Login failed:" in stderr


def test__login_cancel():
    with Popen(
        [
            "python",
            "-m" "oidc_client",
            "--config-file",
            "./tests/profiles.toml",
            "--profile",
            "test_gitlab",
            "--debug",
            "login",
            "--client-secret-stdin",
        ],
        stdin=PIPE,
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        proc.send_signal(signal.SIGINT)
